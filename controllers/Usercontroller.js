const io = require('../io');
const requestJson = require('request-json');
const crypt =require('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuvrf9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;



function getUsersV1(req,res) {
  console.log('GET /apitechu/v1/users');
  //res.sendFile('USUARIOS.json', {root: __dirname});
  //otra forma
 var result = {};
// var users  = require('../USUARIOS.json') ;//añado un . pq estoy en otra ruta
 var users  = require('../usuariospass.json') ;
  //res.send(users);
 var top = req.query.$top;
 var count = req.query.$count;
 if (top && count){
  console.log("recupero top ");
  var userslice = users.slice(0,top);
  console.log("contar");
  //userslice.count = users.length;
  var userscount = users.length;
  result.count=userscount;
  result.users=userslice;
  //userslice.push({"count" : userscount});
  //res.send(userslice);
  res.send(result);
} else if (top){
  console.log("recupero top ");
  var userslice = users.slice(0,top);
  result.users=userslice;
  res.send(result);
} else if (count){
  var userscount = users.length;
  result.count=userscount;
  result.users=users;
  res.send(result);
  //users.push({"count" : userscount});
  //res.send(users);
  }else{
   result.users=users;
   res.send(result);
        }
}

module.exports.getUsersV1 = getUsersV1;

function getUsersV2(req,res) {
  console.log('GET /apitechu/v2/users');


  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body :{
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
)



}

module.exports.getUsersV2 = getUsersV2;

function getUserByIdV2(req,res){
  console.log('GET /apitechu/v2/users/id');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer " + id);
  var query = 'q={"id":' + id + '}';

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body){
    if (err){
      var response = {
      "msg" : "Error obteniendo usuario"
      }
      res.status(500);
    } else {
      if (body.length > 0){
        var response = body[0];
      } else {
        var response = {
          "msg" : "Usuario no encontrado"
        }
        res.status(404);
      }
    }
    res.send(response);
  }
)
}


module.exports.getUserByIdV2 = getUserByIdV2;

function createUserV1(req,res) {
console.log('POST /apitechu/v1/users');
//  console.log(req.headers);
//  console.log(req.headers.first_name);
//  console.log(req.headers.last_name);
//console.log(req.headers.email);

//var newUser = {
  //"first_name": req.headers.first_name,
  //"last_name": req.headers.last_name,
  //"email" : req.headers.email
  //}
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email" : req.body.email
    }
var users = require ('../USUARIOS.json');
users.push(newUser);
console.log("usuario añadido");
io.writeUserDataToFile(users);

//  const fs =require('fs');
//  var jsonUserData = JSON.stringify(users);
//  fs.writeFile("./usuarios2.json", jsonUserData, "utf8",
//     function(err) {
//       if (err){console.log(err);
//       }else{console.log("Usuario persistido");
//       }
//     }
//)
}
module.exports.createUserV1 = createUserV1;


function createUserV2(req,res) {
  console.log('POST /apitechu/v2/users');
  console.log("id es " +req.body.id)
  console.log("first_name es " +req.body.first_name);
  console.log("last_name es " +req.body.last_name);
  console.log("email es " +req.body.email);
  console.log("password es " +req.body.password);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var newUser = {
    "id" : req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario guardado con éxito");
      res.status(201).send({"msg" : "Usuario guardado con éxito"})
    }
  )



}

module.exports.createUserV2 = createUserV2;


function deleteUserV1(req,res){
console.log('DELETE /apitechu/v1/users/:id');
console.log("La id del elemento a borrar es " + req.params.id)
//versión simple de borrado
 var users = require('../USUARIOS.json');
  //users.splice(req.params.id - 1, 1);
  //console.log("Usuario borrado");
  //writeUserDataToFile(users);
  for (var i=0 ; i< users.length; i++){
    console.log(users[i].id);
      if (req.params.id == users[i].id) {
          users.splice(i, 1);
          console.log("Usuario borrado");
         break;
      }else{console.log("Usuario no es")}
   }
  io.writeUserDataToFile(users);//refactorización
}
module.exports.deleteUserV1 = deleteUserV1;
