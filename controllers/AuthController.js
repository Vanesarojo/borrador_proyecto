const io = require('../io');
const requestJson = require('request-json');
const crypt =require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuvrf9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req,res) {
  console.log('POST /apitechu/v1/login');
  console.log(req.body.email);
  console.log(req.body.password);
  var userLogin = {
    "email" : req.body.email,
    "password" : req.body.password
    }
  var users = require ('../usuariospass.json');
  var logged = false;
  var loggedId;
  for (var i=0 ; i< users.length; i++){
    console.log(users[i].email);
      if (req.body.email == users[i].email &&
          req.body.password == users[i].password) {
          logged = true;
          loggedId = users[i].id;
          console.log("Usuario correcto");
          users[i].logged = logged;
          io.writeUserDataToFile(users);
          console.log("usuario añadido");
         break;
      }
   }
   var msg = logged ?
   "Usuario logado " : "Usuario no encontrado.";

   console.log(msg);
   var response = {
    "mensaje": msg,
    "idUsuario": loggedId
};

   res.send(response);


}
module.exports.loginV1 = loginV1;

function loginV2(req,res) {
  console.log('POST /apitechu/v2/login');
  console.log(req.body.email);
  console.log(req.body.password);
  var userLogin = {
    "email" : req.body.email,
    "password" : req.body.password
    }

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente created");

  var query = 'q={"email": "' + req.body.email + '"}';
  var found = false;
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body){
    if (err){
      var response = {
      "msg" : "Error obteniendo usuario"
      }
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0){
        var response = body[0];
        var passBody = req.body.password;
        var passDB = response.password;
        var loggedId = response.id;

        console.log(loggedId);
        console.log("response.password" +response.password +"..");
        console.log("req.body.passwor" +req.body.password +".");
        found = crypt.checkPassword(req.body.password,response.password);
        console.log("found" + found);
        //if (passBody == passDB){
        if (found){
             console.log("Usuario logado");
             var putBody = '{"$set" :{"logged" : true}}';
             httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              var response = {
                "msg" : "Usuario logado con éxito",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          )

        } else {
          var response = {
            "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
          }
          res.status(401);
          res.send(response);
        }
  }else{
    var response = {
      "mensaje" : "Usuario no encontrado"
    }
    res.status(404);
    res.send(response);
  }


}

})
}
  module.exports.loginV2 = loginV2;

function logoutV1(req,res) {
  console.log('POST /apitechu/v1/logout/:id');
  console.log("La id del usuarioa desconectar es " + req.params.id)
  var users = require ('../usuariospass.json');
  var logout = false;
  for (var i=0 ; i< users.length; i++){
    console.log(users[i].email);
      if (req.params.id == users[i].id && users[i].logged === true) {
          logout = true;
          console.log("Usuario deslogado");
          delete users[i].logged;
          io.writeUserDataToFile(users);
          console.log("usuario añadido");
         break;
      }
   }
   var msg = logout ?
   "logout correcto "  : "logout incorrecto.";


   var response = {
    "mensaje": msg,
    "idUsuario": req.params.id
};

   res.send(response);
}
module.exports.logoutV1 = logoutV1;

function logoutV2(req,res) {
  console.log('POST /apitechu/v2/logout/:id');
  console.log("La id del usuario a desconectar es " + req.params.id)
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente created");
  var query = 'q={"id":' + req.params.id + '}';

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body){
    if (err){
      console.log("err");
      var response = {
      "msg" : "Error obteniendo usuario"
      }
      res.status(500);
      res.send(response);
    } else {
      console.log("body");
      if (body.length > 0){
        var response = body[0];
        if (req.params.id == response.id && response.logged === true) {
          console.log("encontrado");
          var putBody = '{"$unset": {"logged": ""}}';
           console.log(putBody);
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err, resMLab, body){
             if (err){
               var response = {
               "msg" : "Error logout"
               }
               console.log(resMLab);
               res.send(response);
             } else {
                 console.log("Usuario deslogado");

                 var response = {
                 "msg" : "logout correcto",
                 "idUsuario": req.params.id
                 }
                 console.log(response);
                 res.send(response);
               }

           }
         )
      } else{
        console.log("aqui");
        var response = {
          "msg" : "logout incorrecto."
        }
        res.status(401);
        res.send(response);
      }
    } else {
      console.log("Usuario no encontrado");
      var response = {
        "msg" : "Usuario no encontrado"
      }
      res.status(404);
      res.send(response);
    }
    }
    //res.send(response);
}

)
}
module.exports.logoutV2 = logoutV2;
