const requestJson = require('request-json');
const crypt =require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuvrf9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountByUserId(req,res){
  console.log('GET /apitechu/v2/account/id');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");
  var id = req.params.id;
  console.log("Id del usuario a traer " + id);
  var query = 'q={"userid":' + id + '}';

  httpClient.get("Account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body){
    if (err){
      var response = {
      "msg" : "Error obteniendo usuario"
      }
      res.status(500);
    } else {
      if (body.length > 0){
        var response = body;
      } else {
        var response = {
          "msg" : "Usuario no encontrado"
        }
        res.status(404);
      }
    }
    res.send(response);
  }
)







}

module.exports.getAccountByUserId = getAccountByUserId;
