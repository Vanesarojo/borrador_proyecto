require('dotenv').config();

const express = require('express'); // carga el framework
const app = express();// inicia el framework
const io = require('./io');
const userController = require('./controllers/Usercontroller');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

var enableCORS = function(req, res, next) {
res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
res.set("Access-Control-Allow-Headers", "Content-Type");

next();
}
app.use(express.json()); //para que funcione el post monstruo
app.use(enableCORS);

const port = process.env.PORT || 3000; // define el puerto
app.listen (port); //arranca el servidor
console.log("API escuchando en el puerto BIP BIP" + port);
//
app.get('/apitechu/v1/hello', //lo usamos como endpoint OJO!!! no olvidar la coma
// funcion de petición y respuesta
function(req,res){
  console.log("GET /apitechu/v1/hello");

//  res.send('Hola desde API TechU');  esto es un string y lo traduce HTML
//  res.send('{"msg" : "Hola desde API TechU"}'') esto es un HTML convertible a JSON
// mando un JSON
res.send({"msg" : "Hola desde API TechU"});
}
)
app.post('/apitechu/v1/monstruo/:p1/:p2',
function (req,res) {
  console.log("Parámetros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);
}
)
app.get('/apitechu/v1/users', userController.getUsersV1);
// el contenido lo refactorizo en Usercontroller.js
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);


app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);//recibe el parametro id


//estamos fuera de app


 //function deleteUserToFile(pos,users){
//   console.log("deleteUserToFile");
   //for normal
  //    for (var i=0 ; i< users.length; i++){
    //    console.log(users[i].id);
      //    if (pos == users[i].id) {
        //     console.log("for; + pos");
          //   users.splice(i, 1);
      //        console.log("Usuario borrado");
        //     break;
  //        }else{console.log("Usuario no es")}
    //   }
    //Array for each
    //  users.forEach(function (element,index){
      //  console.log(element,index);
    //    if (pos == element.id){
    //        console.log("for; " + pos );
          //  console.log(element.id);
    //       users.splice(index, 1);
    //       console.log("Usuario borrado");
    //       }else{console.log("Usuario no es")}
    //   })
    //for (element in object)
  //    }

  //LOGIN - LOGOUT
  app.post('/apitechu/v1/login', authController.loginV1);
  app.post('/apitechu/v2/login', authController.loginV2);
  app.post('/apitechu/v1/logout/:id', authController.logoutV1);
  app.post('/apitechu/v2/logout/:id', authController.logoutV2);
  app.get('/apitechu/v2/account/:id', accountController.getAccountByUserId);
