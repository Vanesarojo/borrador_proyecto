const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
describe('First test',
  function (){
    it('Test that DuckDuckgo works', function(done) {
        chai.request('http://www.duckduckgo.com')
      //chai.request('https://developer.mozilla.org/en-US/adsfasdfas') para probar el error del test
        .get('/')
        .end(
          function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }

)
describe('Test de API usuarios',
  function (){
    it('Prueba que la API de usuarios responde correctamente', function(done) {
        chai.request('http://localhost:3000')
      //chai.request('https://developer.mozilla.org/en-US/adsfasdfas') para probar el error del test
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU");
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos', function(done) {
        chai.request('http://localhost:3000')
      //chai.request('https://developer.mozilla.org/en-US/adsfasdfas') para probar el error del test
        .get('/apitechu/v1/users')
        .end(
          function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            res.should.have.status(200);
            res.body.users.should.be.a('array');//comprueba que sea tipo array

            for (user of res.body.users) {
              user.should.have.property('email');
              user.should.have.property('first_name');
            }
            done();
          }
        )
      }
    )
  }

)
